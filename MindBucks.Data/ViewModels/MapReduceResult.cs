﻿namespace MindBucks.Data.ViewModels
{
    public class MapReduceResult<T>
    {
        public object _id { get; set; }
        public T value { get; set; }
    }
}