﻿using System.Collections.Generic;

namespace MindBucks.Data.ViewModels
{
    public class DashboardViewModel
    {
        public IEnumerable<StrikeDashboardViewModel> Strikes { get; set; } 
    }
}