﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.ViewModels;
using ShortBus;

namespace MindBucks.Data.Queries
{
    public class StrikeCommand : IRequest<CommandResultViewModel>
    {
        public string Email { get; set; }
        public string RequesterEmail { get; set; }
        public string RequesterDisplayName { get; set; }
    }
}
