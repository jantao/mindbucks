﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.Model;
using MindBucks.Data.ViewModels;
using ShortBus;

namespace MindBucks.Data.Queries.Handlers
{
    public class StrikeCommandHandler : IRequestHandler<StrikeCommand, CommandResultViewModel>
    {
        private readonly IDatabaseContext _dbContext;

        public StrikeCommandHandler(IDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CommandResultViewModel Handle(StrikeCommand request)
        {
            var sameUser = request.RequesterEmail.Equals(request.Email, StringComparison.InvariantCultureIgnoreCase);
            if (!sameUser)
            {
                //rigth now only user can strike it's own
                return CommandResultViewModel.CreateError("Cannot strike other person rigth now");
            }
            var date = DateTime.Now;
            var strike = new Strike
            {
                Date = date,
                Email = request.Email,
                CreatedBy = request.Email,
                Active = sameUser,
                Adding = !sameUser
            };
            strike.StrikeHistory.Add(new StrikeHistory
            {
                Date = date,
                Email = request.Email,
                DisplayName = request.RequesterDisplayName,
                Action = "Added"
            });

            _dbContext.Strikes.Insert(strike);

            return CommandResultViewModel.CreateSuccess();
        }
    }
}
