﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.Model;
using MindBucks.Data.ViewModels;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ShortBus;

namespace MindBucks.Data.Queries.Handlers
{
    public class StrikesPerUserQueryHandler : IRequestHandler<StrikesPerUserQuery, IEnumerable<StrikeDashboardViewModel>>
    {
        private readonly IDatabaseContext _dbContext;

        public StrikesPerUserQueryHandler(IDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<StrikeDashboardViewModel> Handle(StrikesPerUserQuery request)
        {
            var strikes = _dbContext.Strikes
    .MapReduce(new MapReduceArgs
    {
        MapFunction = @"
function() {
    var strike = this;
    var count = 0;
    var cashedIn = 0;
    if(!strike.CashedIn){
        count = 1;
    } else {
        cashedIn = 1;
    }
    
    
    emit(strike.Email, { Email: strike.Email, Strikes: count, Cash: cashedIn });
}
",
        ReduceFunction = @"
function(key, values) {
    var result = { Email: key, Strikes: 0, Cash: 0 };

    values.forEach(function(value){
        result.Strikes += value.Strikes;
        result.Cash += value.Cash;
    });
    
    return result;
}
",
        Query = Query<Strike>.Where(s => s.Active)
    }).GetResultsAs<MapReduceResult<StrikeDashboardViewModel>>()
    .Select(s => s.value);

            return strikes;
        }
    }
}
