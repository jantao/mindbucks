﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.Model;
using MindBucks.Data.ViewModels;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using ShortBus;

namespace MindBucks.Data.Queries.Handlers
{
    public class CashInCommandHandler : IRequestHandler<CashInCommand, CommandResultViewModel>
    {
        private readonly IDatabaseContext _dbContext;

        public CashInCommandHandler(IDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CommandResultViewModel Handle(CashInCommand request)
        {
            var availableList = _dbContext.Strikes
                .AsQueryable<Strike>()
                .Where(s => s.Email == request.Email && s.Active && !s.CashedIn)
                .OrderBy(s => s.Date)
                .Take(request.Ammount)
                .ToList();

            if (request.Ammount > availableList.Count)
            {
                var availableStrikes = availableList.Count;
                return CommandResultViewModel.CreateError(string.Format("You don't have {0} strikes available. Only {1}",
                        request.Ammount, availableStrikes));
            }

            var update = new UpdateBuilder<Strike>().Set(s => s.CashedIn, true);
            var updateSelector = Query<Strike>.In(s => s._id, availableList.Select(s => s._id));
            _dbContext.Strikes.Update(updateSelector, update, UpdateFlags.Multi);
            return CommandResultViewModel.CreateSuccess();
        }
    }
}
