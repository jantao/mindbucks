﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.Model;
using MindBucks.Data.ViewModels.CashIn;
using MongoDB.Driver.Linq;
using ShortBus;

namespace MindBucks.Data.Queries.Handlers
{
    public class StrikesAvailableForCashQueryHandler : IRequestHandler<StrikesAvailableForCashQuery, CashInViewModel>
    {
        private readonly IDatabaseContext _dbContext;

        public StrikesAvailableForCashQueryHandler(IDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CashInViewModel Handle(StrikesAvailableForCashQuery request)
        {
            var userEmail = request.Email;
            var available = _dbContext.Strikes
                .AsQueryable()
                .Count(s => s.Email == userEmail && s.Active && !s.CashedIn);
            var defaultAmmount = available - available % 5;
            return new CashInViewModel { Available = available, Ammount = defaultAmmount };
        }
    }
}
