﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindBucks.Data.ViewModels;
using ShortBus;

namespace MindBucks.Data.Queries
{
    public class CashInCommand : IRequest<CommandResultViewModel>
    {
        public string Email { get; set; }
        public int Ammount { get; set; }
    }
}
