﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using MindBucks.Data.Mongo;
using ShortBus;

namespace MindBucks.Data
{
    public class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            string connstring = ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString;
            //app harbor connection settings injection
            if (ConfigurationManager.AppSettings.Get("MONGOHQ_URL") != null)
            {
                connstring = ConfigurationManager.AppSettings.Get("MONGOHQ_URL");
            }
            if (ConfigurationManager.AppSettings.Get("MONGOLAB_URI") != null)
            {
                connstring = ConfigurationManager.AppSettings.Get("MONGOLAB_URI");
            }
            
            builder.RegisterType<DatabaseContext>()
                .As<IDatabaseContext>()
                .WithParameter("connString", connstring)
                .InstancePerDependency();

            //register all mediator requests
            builder.RegisterAssemblyTypes(typeof(IocModule).Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .AsImplementedInterfaces();
        }
    }
}
