﻿using System;
using System.Configuration;
using MindBucks.Data.Model;
using MongoDB.Driver;

namespace MindBucks.Data.Mongo
{
    public class DatabaseContext : IDatabaseContext
    {
        private readonly MongoDatabase _mongoDatabase;

        public DatabaseContext(string connString)
        {
            _mongoDatabase = GetDatabase(connString);
        }

        protected MongoDatabase GetDatabase(string connStringName)
        {
            if (connStringName.ToLower().StartsWith("mongodb://"))
            {
                return GetDatabaseFromUrl(new MongoUrl(connStringName));
            }
            return GetDatabaseFromSqlStyle(connStringName);
        }

        /// <summary>
        ///     Gets the database from connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>MongoDatabase.</returns>
        /// <exception cref="System.Exception">No database name specified in connection string</exception>
        private MongoDatabase GetDatabaseFromSqlStyle(string connectionString)
        {
            var conString = new MongoConnectionStringBuilder(connectionString);
            MongoClientSettings settings = MongoClientSettings.FromConnectionStringBuilder(conString);
            MongoServer server = new MongoClient(settings).GetServer();
            if (conString.DatabaseName == null)
            {
                throw new Exception("No database name specified in connection string");
            }
            return server.GetDatabase(conString.DatabaseName);
        }

        /// <summary>
        ///     Gets the database from URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns>MongoDatabase.</returns>
        private MongoDatabase GetDatabaseFromUrl(MongoUrl url)
        {
            var client = new MongoClient(url);
            MongoServer server = client.GetServer();
            if (url.DatabaseName == null)
            {
                throw new Exception("No database name specified in connection string");
            }
            return server.GetDatabase(url.DatabaseName); // WriteConcern defaulted to Acknowledged
        }

        /// <summary>
        ///     Uses connectionString to connect to server and then uses databae name specified.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="dbName">Name of the database.</param>
        /// <returns>MongoDatabase.</returns>
        private MongoDatabase GetDatabase(string connectionString, string dbName)
        {
            var client = new MongoClient(connectionString);
            MongoServer server = client.GetServer();
            return server.GetDatabase(dbName);
        }

        public MongoCollection<Strike> Strikes
        {
            get { return CreateOrGetCollection<Strike>("strike"); }
        }

        public MongoCollection Users
        {
            get { return CreateOrGetCollection("AspNetUsers"); }
        }

        private MongoCollection<T> CreateOrGetCollection<T>(string collectionName)
        {
            if (!_mongoDatabase.CollectionExists(collectionName))
            {
                _mongoDatabase.CreateCollection(collectionName);
            }
            return _mongoDatabase.GetCollection<T>(collectionName);
        }

        private MongoCollection CreateOrGetCollection(string collectionName)
        {
            if (!_mongoDatabase.CollectionExists(collectionName))
            {
                _mongoDatabase.CreateCollection(collectionName);
            }
            return _mongoDatabase.GetCollection(collectionName);
        }


        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                _mongoDatabase.Server.Disconnect();
            }
            _disposed = true;
        }
    }
}
