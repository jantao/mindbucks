﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MongoDB.Bson;

namespace MindBucks.Data.Model
{
    public class Strike
    {
        public DateTime Date { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public bool CashedIn { get; set; }
        public bool Active { get; set; }
        public bool Discarted { get; set; }

        public string CreatedBy { get; set; }
        public string RemovedBy { get; set; }
        public bool Removing { get; set; }
        public bool Adding { get; set; }

        public ICollection<StrikeHistory> StrikeHistory { get; set; }

        public ObjectId _id { get; set; }

        public Strike()
        {
            StrikeHistory = new Collection<StrikeHistory>();
        }
    }

    public class StrikeHistory
    {
        public DateTime Date { get; set; }
        public string Email { get; set; }

        public string DisplayName { get; set; }
        public string Action { get; set; }
    }
}