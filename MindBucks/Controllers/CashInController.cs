﻿using System.Web.Mvc;
using MindBucks.Data;
using MindBucks.Data.Queries;
using MindBucks.Data.ViewModels.CashIn;
using MindBucks.Helpers;
using ShortBus;

namespace MindBucks.Controllers
{
    public class CashInController : Controller
    {
        private readonly IMediator _mediator;

        public CashInController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public ActionResult CashIn()
        {
            var response = _mediator.Request(new StrikesAvailableForCashQuery {Email = User.Identity.GetEmail()});
            return View(response.Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CashIn(CashInViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var userEmail = User.Identity.GetEmail();
            var response = _mediator.Request(new CashInCommand {Ammount = model.Ammount, Email = userEmail});

            if (response.Data.Success)
            {
                return RedirectToAction("Index", "Home");    
            }
            
            ModelState.AddModelError("", response.Data.ErrorMessage);
            return View(model);
        }
    }
}