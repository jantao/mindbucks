﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MindBucks.Data;
using MindBucks.Data.Model;
using MindBucks.Data.Queries;
using MindBucks.Data.ViewModels;
using MindBucks.Helpers;
using MindBucks.Models;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Wrappers;
using ShortBus;

namespace MindBucks.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public ActionResult Index()
        {
            var dashboarViewModel = _mediator.Request(new DashboardQuery()).Data;
            return View(dashboarViewModel);
        }

        public ActionResult Strike(string email)
        {
            var userEmail = User.Identity.GetEmail();
            var displayName = User.Identity.GetDisplayName();
            var result = _mediator.Request(new StrikeCommand
            {
                Email = email,
                RequesterEmail = userEmail,
                RequesterDisplayName = displayName
            });

            if (result.Data.Success)
            {
                return RedirectToAction("Index");
            }
            //handle error
            return RedirectToAction("Index");
        }



    }
}