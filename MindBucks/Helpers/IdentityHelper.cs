﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web;

namespace MindBucks.Helpers
{
    public static class IdentityHelper
    {
        public static string GetDisplayName(this IIdentity identity)
        {
            var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            return prinicpal.Claims.Where(c => c.Type == "DisplayName").Select(c => c.Value).FirstOrDefault();
        }

        public static string GetEmail(this IIdentity identity)
        {
            var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            return prinicpal.Claims.Where(c => c.Type == "Email").Select(c => c.Value).FirstOrDefault();
        }
    }
}