﻿using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MindBucks.Data.Model;
using MongoDB.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MindBucks.Models;

namespace MindBucks
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            string connstring = ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString;
            //app harbor connection settings injection
            if (ConfigurationManager.AppSettings.Get("MONGOHQ_URL") != null)
            {
                connstring = ConfigurationManager.AppSettings.Get("MONGOHQ_URL");
            }
            if (ConfigurationManager.AppSettings.Get("MONGOLAB_URI") != null)
            {
                connstring = ConfigurationManager.AppSettings.Get("MONGOLAB_URI");
            }

            var store = new UserStore<ApplicationUser>(new IdentityDbContext(connstring));
            var manager = new ApplicationUserManager(store);
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                //RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            manager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is: {0}"
            });
            manager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is: {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            Seed(manager);
            return manager;
        }

        private static void Seed(ApplicationUserManager manager)
        {
            if ((manager.FindByName("j.silva@mindbus.nl")) != null)
            {
                return;
            }

            var users = new []
            {
                new ApplicationUser {Email = "j.silva@mindbus.nl", UserName = "j.silva@mindbus.nl", DisplayName = "Joao"},
                new ApplicationUser {Email = "diogo@mindbus.nl", UserName = "diogo@mindbus.nl", DisplayName = "Diogo"},
                new ApplicationUser {Email = "nuno@mindbus.nl", UserName = "nuno@mindbus.nl", DisplayName = "Nuno"},
                new ApplicationUser {Email = "carlos@mindbus.nl", UserName = "carlos@mindbus.nl", DisplayName = "Carlos"} /*,
                new ApplicationUser {Email = "kappy@acydburne.com.pt", UserName = "kappy@acydburne.com.pt", DisplayName = "Kappy"}*/
            };
            Task.WhenAll(users.Select(manager.CreateAsync));
        }
    }

    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your sms service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
